# Spring Boot Unsecure Example
## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-unsecure-example.git`.
2. Go to the folder: `cd springboot-unsecure-example`.
3. Run the application: `mvn clean spring-boot:run`.
4. Open your favorite browser: http://localhost:8080

## Screen shot

Admin Page

![Admin Page](img/admin.png "Admin Page")

Home Page

![Home Page](img/home.png "Home Page")

Management Page

![Management Page](img/management.png "Management Page")

Profile Page

![Profile Page](img/profile.png "Profile Page")
