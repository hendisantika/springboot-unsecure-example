package com.hendisantika.springbootunsecureexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUnsecureExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUnsecureExampleApplication.class, args);
    }

}
