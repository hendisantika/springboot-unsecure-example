package com.hendisantika.springbootunsecureexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-unsecure-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/03/20
 * Time: 08.47
 */
@Controller
@RequestMapping("profile")
public class ProfileController {

    @GetMapping("index")
    public String index() {
        return "profile/index";
    }
}